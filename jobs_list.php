<?php

$jobs = array(
	        array(
	        	"job" => 'cat sample.txt',
	        	"successes" => 4,
	        	"message" => "Starting Parametric Transient Runs" 
	    	),
	    	array(
	    		"job" => 'farm --jobfile ./farm/Individual_Job_Files/SlowOverLoad_job.file --netlist -sweep -farm --post --force',
        		"successes" => 0,
        		"message" => "Starting SlowOverLoad_job Runs" 
    		),
);