<?php

/**
 * This script runs a set of commands "jobs" and expects to receive a set number of success messages from the cmd output "successes".
 * 
 * Set your job files in the jobs_list.php file along with the number of expected "completed successfully" messages and job message.
 */

include('autoload.php');

$reset = \Config::get("reset_log");
if($reset==true){
   unlink("job_runner.log");
}

foreach ($jobs as $job_object) {
    $message = $job_object["message"];
    $successCount = $job_object["successes"];
    $job = $job_object['job'];

    $job = new JobExector($job, $successCount, $message);
    $job->process();
    $job->shutdown();
}


class JobExector {

	public $job;
	public $successes;
	public $message;
	public $debug;
	public $job_id;
	public $jobRunTime;

	public function __construct($job, $successes, $message){ 
		$this->job = $job;
		$this->successes = $successes;
		$this->message = $message;
		$this->debug = \Config::get("debug");
		$this->setJobHash();
		$this->jobRunTime;
	}

	public function process(){

        logEvent($this->message, $this->job_id);
        
        $output = "";

        $start_time = time();

        logEvent(PHP_EOL."---*******--- BEGIN JOB RESULTS FOR ($this->job_id) ---*******---".PHP_EOL, $this->job_id);
        exec("$this->job", $output);

		$end_time = time();
		$this->jobRunTime = $this->runTimer($start_time, $end_time);

        $outputString = implode(PHP_EOL, $output).PHP_EOL;
        $matches = array();
        preg_match_all('/completed successfully/', $outputString, $matches);

        logEvent($outputString, $this->job_id);
        logEvent("---*******--- END JOB RESULTS FOR ($this->job_id) ---*******---".PHP_EOL, $this->job_id);

        $count = count($matches[0]);

        if($this->debug && $count < 1){
    		logEvent("FAILED! Job ran in $this->jobRunTime. DEBUG MODE: Expected more than 1 and got $count", $this->job_id);
            logEvent("Exiting due to job failure!");
            $this->shutdown(true);
        }
        elseif(!$this->debug && $count !== $this->successes){
    		logEvent("FAILED! Job ran in $this->jobRunTime. Expected $this->successes and got $count", $this->job_id);
            logEvent("Exiting due to job failure!", $this->job_id);
            $this->shutdown(true);
        }
        else
        	$this->debug ? $expecting = 'DEBUG MODE: Expected more than 1' : $expecting = 'Expected '.$this->successes;
            logEvent("SUCCESS! Job ran in $this->jobRunTime. $expecting and got $count", $this->job_id, true);
	}

	private function setJobHash() {
	    $length = 6;
	    $today = date("H.i.s.u");
	    $this->job_id = substr(hash('sha1', $today), 0, $length);
	}

	private function runTimer($date_1 , $date_2) {
   
	    $minutes = round(abs($date_1 - $date_2) / 60,2);
		return "$minutes minute";

	}

    public function shutdown($forced = false){

        if(\Config::get("send_email") == true){
            $mailer = new Mailer();
            if($forced == true)
                $mailer->send('JobRunner Failed!','JobRunner failed to complete successfully.','job_runner.log');
            else
                $mailer->send('JobRunner Success!','JobRunner completed successfully.','job_runner.log');
        }

        if($forced) exit(1);
    }

}
