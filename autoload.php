<?php

/**
 * This is the bootloader for our app. It loads all necessary files to make the app work.
 * 	
 * Include all of the src files
 *
 */
foreach (glob("src/*.php") as $filename)
{
    include $filename;
};

require 'vendor/autoload.php';

/**
 * 
 * Pull in the job_list file.	
 *
 */
include('jobs_list.php');
