<?php

if (!function_exists('array_get'))
{
	/**
	 * Get an item from an array using "dot" notation.
	 *
	 * @param  array   $array
	 * @param  string  $key
	 * @param  mixed   $default
	 * @return mixed
	 */
	function array_get($array, $key, $default = null)
	{
		if (is_null($key)) return $array;

		if (isset($array[$key])) return $array[$key];

		foreach (explode('.', $key) as $segment)
		{
			if ( ! is_array($array) || ! array_key_exists($segment, $array))
			{
				echo 'Config::get key does not exist!';
			}

			$array = $array[$segment];
		}

		return $array;
	}

    function logEvent($message, $job_id = null, $email_update = false) {
		$date = date("Y-m-d H:i:s");
		$log_message = '['.$date."] JOB_RUNNER($job_id) - ".$message.PHP_EOL;

		if($email_update){
			$exec = exec('df -h /sim/DAEDALUS_OA_DS/ | awk \'{print $(NF-1)}\' | awk \'NR==2\'');
			$log_message = $log_message.'Mount Usage: '.$exec.PHP_EOL;
		}

		echo $log_message;

		file_put_contents('job_runner.log', $log_message, FILE_APPEND);

		if(\Config::get("send_email_updates") == true && $email_update){
			$mailer = new Mailer();
			$mailer->send('JobRunner Update!', $log_message);
        }
	}
}