<?php

class Mailer {

    public function __construct(){

	}

    /**
     * Send an email
     */
    public function send($subject, $body, $attachment = null){

        $mail = new PHPMailer();

        if(\Config::get("email_settings.use_smtp") == true){
            $mail->isSMTP();
            $mail->Host = \Config::get("email_settings.smtp_host");
            $mail->SMTPAuth = true;
            $mail->Username = \Config::get("email_settings.smtp_username");
            $mail->Password = \Config::get("email_settings.smtp_password");
            $mail->SMTPSecure = \Config::get("email_settings.smtp_secure");;
            $mail->Port = \Config::get("email_settings.smtp_port");;
        }

        $mail->From = \Config::get("email_settings.from_address");
        $mail->FromName = \Config::get("email_settings.from_name");
        $mail->addAddress(\Config::get("email_settings.to_address"), \Config::get("email_settings.to_name"));

        $mail->WordWrap = 50;

        if(!is_null($attachment))
            $mail->addAttachment($attachment);

        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $body;

        if(!$mail->send()) {
            logEvent('Email could not be sent.'.PHP_EOL);
            logEvent('Mailer Error: ' . $mail->ErrorInfo);
        } else {
            logEvent('Email has been sent'.PHP_EOL);
        }
   }
}