<?php

class Config{

	/**
	 * Get the specified configuration value.
	 *
	 * @param  string  $key
	 */
	public static function get($key){

		include("config.php");
		return array_get($config, $key);

	}
}