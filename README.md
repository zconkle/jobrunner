# JOBrunner - A full-featured job manager

With JOBrunner you can create a list of shell commands, called "jobs" and JOBrunner will execute these sequentially if each "job" returns a success message.


## Installation

- Clone this git project locally
- Download composer if you have not already done so: "curl -sS https://getcomposer.org/installer | php"
- Install dependencies via composer: "php composer.phar install"
- Update the job_list.php file with the jobs you would like to run
- Ensure config.php reflects the way you want JOBrunner to manage these jobs
- Run JOBrunner: "php jobrunner.php"