<?php

$config = array(
	
	'debug' 		        => false, //If set expects at-least one success message and moves onto the next job.
	'send_email' 	        => true,
	'send_email_updates'    => true,  //If set to true this will send an email every time a message is logged
	'reset_log'		        => true,  //Resets the log file every time the jobrunner is started

	'email_settings' 	=> array(
		'from_address' 	=> 'someone@example.com',
		'from_name'		=> 'jobRunner',
		'to_address'	=> 'someone@example.com',
		'to_name'		=> 'First Name',
		'use_smtp'		=> false,
        'smtp_host'     => '',
		'smtp_username'	=> '',
		'smtp_password'	=> '',
        'smtp_secure'   => 'tls',
		'smtp_port'		=> 587,
		),

);